import logging
import os
import urllib

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))
NYT_API_KEY = '87267c5caf8c85fff5c5e4dbe20a96de:6:61430921'

logger = logging.getLogger(__name__)

class MainPage(webapp2.RequestHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.headers['Content-Type'] = 'text/html'
        self.response.write(template.render({}))

class NytSearch(webapp2.RequestHandler):
    def get(self):
        query_args = {
            'q': self.request.get('name'),
            'sort': 'newest',
            'api-key': NYT_API_KEY,
            'response-format': 'json', 
        }
        url = 'http://api.nytimes.com/svc/search/v2/articlesearch.json?' + urllib.urlencode(query_args)
        logger.info('GET ' + url)
        result = urllib.urlopen(url)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(result.read())


app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/api/search', NytSearch)
])