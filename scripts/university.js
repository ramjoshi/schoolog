var GOOGLE_API_KEY = 'AIzaSyDyKFW2o-ayRN1JapElktV3SZ62bwwTXo8',
 FREEBASE_JSONP = 'https://www.googleapis.com/freebase/v1/mqlread?callback=?',
 FREEBASE_IMAGE = 'https://www.googleapis.com/freebase/v1/image';

$(function() {
	// Suggest school name from freebase
	$("#school").suggest({
		filter: '(all type:/education/university)',
		key: GOOGLE_API_KEY
	}).bind("fb-select", function(e, data) {

		// retrieve the school's website
		var query = {
			id: data.id,
			'/common/topic/official_website': null,
			'/common/topic/image': null,
			limit: 1
		};
		$.getJSON(FREEBASE_JSONP, {
			query: JSON.stringify(query),
			key: GOOGLE_API_KEY
		}, displayResult);

		// Display the website link
		function displayResult(response) {
			var imgSrc = FREEBASE_IMAGE + data.mid;
			$('#selected-school').attr("href", response.result['/common/topic/webpage']);
			$('#selected img').attr("src", imgSrc);
      $('#alumni').append($('<img></img>').attr('src','/styles/loading-large.gif'));

			// Create a sparql query to get information about the school's alumni
			var sparql = generateAlumniSparqlRequest(data.mid.slice(1).replace('/', '.'));

			// DBPedia URI for executing the sparql query. 
			var dbpediaUrl = "http://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=" + escape(sparql) + "&format=json";
			// An ajax request that requests the above URI and parses the response
			$.ajax({
				dataType :'jsonp',
				jsonp :'callback',
				url :dbpediaUrl,
				success : function(json) {
					var dbPediaAlumni = new Array();
					if(json && 
							json.results && 
							json.results.bindings && 
							json.results.bindings.length > 0) {
						bindings = json.results.bindings;
						dbPediaAlumni = parseBindings(bindings);
					}

					$('#alumni').html('');
          $.each(dbPediaAlumni, function(i, item) {
            alum = $('<div class="alum"></div>');
						img = $('<img></img>').attr('src', item.thumb);
						link1 = $('<a class="shownews"></a>').attr('href', '#');
						link1.append(img);
						alum.append(link1);
						span = $('<span></span').text(item.name);
						link2 = $('<a class="showfbase" target="_blank"></a>').attr('href', item.freebaseUri);
            link2.append(span);
            alum.append(link2);
            alumnus = $('<li class="alumnus"></li>').attr('id', 'alumnus_' + i).append(alum);
            $('#alumni').append(alumnus);
            (function(item) {
                $('#alumnus_' + i + ' a.shownews').click(function(event) {
                    event.preventDefault();
                    loadNytimesLinkedData(item);
                    $('html, body').animate({
                        scrollTop: $('#news').offset().top
                    }, 300);
                });
          	})(item);	
        	});
				}
			});

			// The following method contains code from http://open.blogs.nytimes.com/
			/**
			* This method parses the data requested from DBPedia 
			* about the alumni of a given school.  Since the same Alumnus 
			* may be retured multiple times for a single query, this 
			* method filters out duplicate results.
			*/
			function parseBindings(bindings) {
				var alumni = [];
				var displayIds = [];
				for(var i = 0; i < bindings.length; i++) {
					var binding = bindings[i];
					var bindingUri = binding.alumnus.value;
					if(!(bindingUri in displayIds)) {
						displayIds[bindingUri] = 1;
						var alumnus = new Object;
						alumnus.uri = bindingUri;

						// If we found a freebase URI, use the Freebase 
						// thumbnailing service to generate a thumbnail 
						// image for the alumnus.
						if(binding.freebaseUri) {
							alumnus.thumb = createFreebaseThumbnailUrl(binding.freebaseUri.value);
							alumnus.freebaseUri = binding.freebaseUri.value;
						} else {
							alumnus.thumb = ""; // insert path to default image here
						}
						alumnus.name = binding.name.value;
						alumni.push(alumnus);
					}
				}
				return alumni;
			}

			// The following method contains code from http://open.blogs.nytimes.com/
			/**
			* This method transforms the specific Freebase URI into a request to
			* Freebase's image thumbnailing service.  If such a transformation
			* is not possible, then a default thumbnail URL is returned. 
			*/
			function createFreebaseThumbnailUrl(freebaseUri) {
				var index = freebaseUri.lastIndexOf("/");
				if(index != -1 && index != freebaseUri.length - 1) {
					freebaseUri = freebaseUri.substring(index + 1);
					freebaseUri = freebaseUri.replace(".","/");
					freebaseUri =  FREEBASE_IMAGE + '/' + freebaseUri + '?key=' + GOOGLE_API_KEY;
					return freebaseUri;
				} 
				return ""; // insert path to default image here
			}

			// Creates a SPARQL query to get alumni and their details from DBPedia 
			function generateAlumniSparqlRequest(mid) {

				// The specific DBPedia relations used to identify 
				// somebody as an alumni or employee of a given school. 
				//called it schoolRelations for lack of a better name
				_schoolRelations = [
        	"dbpedia-owl:almaMater"
				];

				var sparql = "SELECT * WHERE {";
				for(var i = 0; i < _schoolRelations.length; i++) {
					sparql += "{?alumnus " + _schoolRelations[i] + " ?schoolUri . " +
					"?alumnus dbpprop:name ?name . " +
					"?alumnus owl:sameAs ?freebaseUri FILTER regex(?freebaseUri,'http://rdf\\\\.freebase\\\\.com/.*') . }";
					if(i < _schoolRelations.length - 1) {
						sparql += " UNION ";
					}
				}
				sparql += ". ?schoolUri owl:sameAs <http://rdf.freebase.com/ns/" + mid + ">.}";
				return sparql;
			}

			// Get news about alumni 
			// Based on code from http://open.blogs.nytimes.com/
			/**
			* This method loads the linked data at http://data.nytimes.com 
			* for each alumnus discovered by DBPedia for a given school. 
			*
			* This method takes advantage of the JSONP support provided 
			* by http://data.nytimes.com and described at 
			* http://data.nytimes.com/home/about.html
			*/
			function loadNytimesLinkedData(alumnus) {
				var query = {
					id: alumnus.freebaseUri.substring(26).replace('.', '/'),
	      	key: {
	      		namespace: '/source/nytimes',
	      		value: null
	      	}
	      };
		    $.getJSON(FREEBASE_JSONP, {
		    	query: JSON.stringify(query),
		      key: GOOGLE_API_KEY
		    }, processNytId);
		    function processNytId(response) {
	        $('#news').empty();
	        $('#news').append($('<h1></h1>').text(alumnus.name + ' in the news.'));
	        if(response.result) {
	        	var nytTopic = response.result.key.value.replace(/\$002F/g, '/');
	          loadAlumnusDetails(alumnus.name, nytTopic);
	        }
	        else {
	            querySearchApi(alumnus.name);
	        }
		    }
			}

			// Contains code from http://open.blogs.nytimes.com/
			/**
			* This method extracts the "skos:prefLabel" attribute from the 
			* RDF-JSON returned by data.nytimes.com and uses this label to query
			* The New York Times Search API 
			* (details: http://developer.nytimes.com/docs/article_search_api). Please
			* note that the "skos:prefLabel" attribute corresponds to a tag returned
			* by the time tags api (http://developer.nytimes.com/docs/timestags_api). 
			*/
			function loadAlumnusDetails(name, topic) {
					
				// Obtain and set the topic page link to an alumnus' 
				// times topics page (more: http://topics.nytimes.com) 
				var topicPageUrl = 'http://topics.nytimes.com/' + topic;
				if(topicPageUrl) {
					$('#news').append($('<a></a>').attr('href', topicPageUrl).text('Times page'));
				}
				// Query the NYT search API for the specified name.
				querySearchApi(name);
			}

			// This ajax request is handled by our own server because New York Times search api doesn't handle client side callback requests
			function querySearchApi(name) {
				var url = url = "api/search?name="+ escape(name);
				$.ajax( {
					dataType :'json',
					json :'callback',
					url : url,
					success : function(json) {
						if(json.status === 'OK') {
							// Display news articles
							displayAlumnusDetails(json);
						}
					}
				});
			}

			/**
			* Renders the output of The New York Times search API as HTML.
			*/
			function displayAlumnusDetails(json) {
				var docs = json.response.docs;
				var html = "";
				for(var i =0; i < docs.length; i++) {
					var id = i;
					var result = docs[i];
					var headline = result.headline.main;
					var date = result.pub_date;
					var url = result.web_url;
					var abstract_text = result.abstract;
					var byline = result.snippet;
					var thumbnail = result.multimedia[0] && result.multimedia[0].url;
					var divId = "headline_" + i; 
					var articleDivId = 'article_' + i;
					html += 
					"<div class='news-article'><div id = '" + 
					divId + 
					"' class='list_headline'><a href='" + url + "' target='_blank'>" + 
					headline + 
					"<\/a>&nbsp;-&nbsp;<span class ='list_dateline'>" + 
					formatDate(date) + 
					"<\/span><\/div>";
					html += '<div id = "'  + articleDivId + '" class="article ' + articleDivId + '_class">';
					if(byline != null) {
						html += '<div class="byline">'+byline+'<\/div>';
					}
					if(abstract_text != null) {
						html += '<div class="abstract_text">'+abstract_text+' ...<\/div>';
					}
					html += '<\/div><\/div>';
				}
				// Display the HTML for a given alumnus.
				$('#news').append(html);
			}

			// Code from http://open.blogs.nytimes.com/
			/**
			* Function that converts a date in YYYY-MM-DD format to written format.
			* For example this method would convert 1979-08-07 to 'August 7, 1979.
			*/
			function formatDate(date) {
				date = date + "";
				if(date.length == 8) {
					year = date.substr(0,4);
					month = date.substr(4,2);
					day = date.substr(6,2);
					if(month == '01') {
						month = 'January';
					} else if(month == '02') {
						month = 'February';
					} else if(month == '03') {
						month = 'March';
					} else if(month == '04') {
						month = 'April';
					} else if(month == '05') {
						month = 'May';
					} else if(month == '06') {
						month = 'June';
					} else if(month == '07') {
						month = 'July';
					} else if(month == '08') {
						month = 'August';
					} else if(month == '09') {
						month = 'September';
					} else if(month == '10') {
						month = 'October';
					} else if(month == '11') {
						month = 'November';
					} else if(month == '12') {
						month = 'December';
					}
					date = month + " " + day + ", " + year; 
				}
				return date;
			}
		}
	});
});
